package main

import (
	tb "gopkg.in/tucnak/telebot.v2"
	"github.com/joho/godotenv"
	"encoding/base64"
	"encoding/json"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strings"
	"bytes"
	"time"
	"fmt"
	"log"
	"os"
)

type Event struct {
	ID int `json:"id"`
	StartDate string `json:"start_date"`
	EndDate string `json:"end_date"`
	Title string `json:"title" db:"title"`
	Description string `json:"description"`
	Email string `json:"email"`
	Phone int `json:"phone"`
	Website string `json:"website"`
	Address string `json:"address"`
	CategoryName string `json:"category_name"`
	Files []File `json:"files"`
	Promoted bool `json:"promoted"`
	CalendarName string `json:"calendar_name"`
	IsFullDay bool `json:"is_full_day"`
}

type File struct {
	ID int `json:"id"`
	Image string `json:"image"`
	Extension string `json:"extension"`
}

type Response struct {
	Values []Event `json:"values"`
}

var (
	token string
	baseURL string
	started bool
)

var days = [...]string {
    "Diumenge", "Dilluns", "Tercer", "Quart", "Cinquè", "Divendres", "Dissabte",
}

var months = [...]string {
    "Gener", "Febrer", "Març", "Abril", "Maig", "Juny",
    "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre",
}

var emojis = [...]string {
	"📚","💃","🕺",
}

func init() {
	e := godotenv.Load()
	if e != nil {
		log.Println(e)
	}

	token = os.Getenv("TELEGRAM_TOKEN")
	baseURL = os.Getenv("BASE_URL")

	started = false
}

func main() {
	b, err := tb.NewBot(tb.Settings{
		URL: "https://api.telegram.org",
		Token: token,
		Poller: &tb.LongPoller{Timeout: 15 * time.Second},
	})

	if err != nil {
		log.Fatal(err)
		return
	}

	ticker := time.NewTicker(time.Second * 60)
	b.Handle(tb.OnChannelPost, func(m *tb.Message) {
		if m.Text == "/start" {
			if !started {
				go func() {
					for range ticker.C {
						now := time.Now()
						target := time.Date(now.Year(), now.Month(), now.Day(), 14, 00, now.Second(), now.Nanosecond(), now.Location())
						if now.Equal(target) {
							t := now.AddDate(0, 0, 1)
							startDate := t.Format("2006-01-02")
							events := GetEvents(startDate)
							
							if events != nil {
								msg := ""
								if len(events[0].Files) > 0 {
									filename := Base64Decode(events[0].Files[0].Image, events[0].Files[0].Extension)

									p := &tb.Photo{File: tb.FromDisk(filename)}
									b.Send(m.Chat, p)

									err = os.Remove(filename)
									if err != nil {
										log.Println(err)
									}
								}

								msg += "🎥 <b>PROPOSTES PER DEMÀ</b> 🎭\n\n"
								msg += "<b>Cinema, xerrades, literatura...</b> La cultura a Balaguer no descansa i demà en tenim per tots els gustos 😋\n\n"
	
								for _, event := range events {
									msg += ToString(event)
								}

								b.Send(m.Chat, msg, &tb.SendOptions{
									ParseMode: tb.ModeHTML,
								})
							}
						}
					}
				}()
				
				started = true
				b.Send(m.Chat, "Successfully started")
			} else {
				b.Send(m.Chat, "Already started")
			}
		} else if m.Text == "/stop" {
			ticker.Stop()
			started = false
			b.Send(m.Chat, "Successfully stopped")
		} else if m.Text == "/getevents" {
			t := time.Now().AddDate(0, 0, 1)
			startDate := t.Format("2006-01-02")
			events := GetEvents(startDate)

			if events != nil {
				msg := ""
				if len(events[0].Files) > 0 {
					filename := Base64Decode(events[0].Files[0].Image, events[0].Files[0].Extension)
					
					p := &tb.Photo{File: tb.FromDisk(filename)}
					b.Send(m.Chat, p)

					err = os.Remove(filename)
					if err != nil {
						log.Println(err)
					}
				}

				msg += "🎥  <b>PROPOSTES PER DEMÀ</b> 🎭\n\n"
				msg += "<b>Cinema, xerrades, literatura...</b> La cultura a Balaguer no descansa i demà en tenim per tots els gustos 😋\n\n"
	
				for _, event := range events {
					msg += ToString(event)
				}

				b.Send(m.Chat, msg, &tb.SendOptions{
					ParseMode: tb.ModeHTML,
				})
			}
		}
	})

	b.Start()
}

func Base64Decode(b64 string, extension string) (string) {
	unbased, err := base64.StdEncoding.DecodeString(b64)
	if err != nil {
		log.Println(err)
	}

	r := bytes.NewReader(unbased)

	switch extension {
		case "png":
			pngI, err := png.Decode(r)
			if err != nil {
				log.Println(err)
			}
			
			filename := "image.png"
			f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE, 0777)
			if err != nil {
				log.Println(err)
			}

			png.Encode(f, pngI)

			return filename
		case "jpeg", "jpg":
			jpgI, err := jpeg.Decode(r)
			if err != nil {
				log.Println(err)
			}
			
			filename := "image.jpg"
			f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE, 0777)
			if err != nil {
				log.Println(err)
			}

			jpeg.Encode(f, jpgI, &jpeg.Options{95})

			return filename
		default:
			log.Println("invalid extension")
	}

	return ""
}

func GetEvents(startDate string) ([]Event) {
	response, err := http.Get(baseURL + "/api/event?filter_by=start_date&query=" + startDate)
	if err != nil {
		log.Fatal(err)
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	var responseObject Response
	json.Unmarshal(responseData, &responseObject)

	return responseObject.Values
}

func ToString(event Event) string {
	layout := "2006-01-02 15:04:05"
	startDate, err := time.Parse(layout, event.StartDate)
	if err != nil {
		log.Fatal(err)
	}

	beautyTime := startDate.Format("15:04") + " h"

	categoryEmoji := emojis[rand.Intn(2)]
	
	return categoryEmoji + "<b>" + event.Title + "</b>" + "\n📆 " + "<b>" + Format(startDate) + "</b>" + "\n🕒 " + beautyTime + "\n📍 " + event.Address + "\n\n"
}

func Format(t time.Time) string {
    return fmt.Sprintf("%s, %02d de %s",
        days[t.Weekday()], t.Day(), strings.ToLower(months[t.Month()-1]),
    )
}