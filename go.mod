module agenda-balaguer-bot

go 1.17

require (
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	gopkg.in/tucnak/telebot.v2 v2.4.0 // indirect
)
