# Agenda Balaguer BOT :robot:

## Requirements
[Go](https://go.dev/dl/)

## Quick Start Guide

### Installation

Clone this repository and chage the current work directory typing the following command line in a terminal.

```
https://gitlab.com/fractal-sl/p0-balaguer/agenda-telegram-bot && cd agenda-telegram-bot
```

Install the project dependencies with the `go install` command.

On the telegram app create a new bot by sending a message to the BotFather `https://t.me/botfather`. When the bot is created a token is generated, put it inside a .env file (you can create if doesnt exist) on the root of the project along with the backend URL. Below is an example of the .env file.

```
TELEGRAM_TOKEN=bot_token
BASE_URL=http://localhost:5000
```

### Running Locally

Change the current work directory to `cmd/agenda-balaguer-bot` with the following command.

```
cd cmd/agenda-balaguer-bot/
```

Run the bot with the `go run main.go` command.

## Example

The bot listen for messages from channels in which it participates, so, make sure the bot is in a channel and has an administrator role.

### Commands

**/start:**
Start the bot task. Every day at 2 pm the bot sends a message with all events of the next day.

**/stop:**
Stop the bot task.

**/getevents:**
Send a message with all events of the next day.

## Heroku Deployment
To deploy the bot on heroku first login on heroku CLI with the `heroku login` command, and then, create a new project typing `heroku create [project_name]`.  
After creating the project add the heroku git URL as a remote on the project repository with the following command.

```
git remote add heroku [heroku git url]
```

Before push to heroku, on the project settings, add the `TELEGRAM_TOKEN` and `BASE_URL` environment variables.

To synchronize the project repository with heroku use the `git push heroku master` command.  
The previous command will send to heroku all the code from master branch.

At the end start the bot with the following command.

```
heroku ps:scale worker=1
```

`Make sure to not run multiple bots with the same telegram token (heroku and locally) to avoid confusion. To run the bot locally you can create a new bot for development or if it suitable you can stop the heroku worker and use the same token locally.`

```
heroku ps:scale worker=0
```